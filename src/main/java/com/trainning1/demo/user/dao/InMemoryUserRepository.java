package com.trainning1.demo.user.dao;

import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.HoraDia;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class InMemoryUserRepository {
    private final Map<Long, HoraDia> state = new HashMap<>();


    public Map<Long, HoraDia> findAll(){
       return state;
    }
    public HoraDia insertOne(Hora hora, Minuto minuto, Segundo segundo){
        Long id = state.size() + 1L;
        HoraDia horaDia = new HoraDia(hora, minuto, segundo, id);
        state.put(id, horaDia);
        return horaDia;
    }
}

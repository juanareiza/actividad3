package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import com.trainning1.demo.user.serialize.IntegerSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class Hora implements IntegerSerialization {
    Integer hora;

    private Hora(Integer hora){
        Precondiciones.conditionNoNul(hora);
        Precondiciones.conditionHora(hora);
        this.hora= hora;
    }

    @Override
    public Integer valueOf() {
        return hora;
    }
}

package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import com.trainning1.demo.user.serialize.IntegerSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class Minuto implements IntegerSerialization {
    Integer minuto;

    private Minuto(Integer minuto){
        Precondiciones.conditionNoNul(minuto);
        Precondiciones.conditionMinuto(minuto);
        this.minuto = minuto;
    }

    @Override
    public Integer valueOf() {
        return minuto;
    }
}

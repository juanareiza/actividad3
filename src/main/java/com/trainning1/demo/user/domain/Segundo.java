package com.trainning1.demo.user.domain;

import com.trainning1.demo.user.precondiciones.Precondiciones;
import com.trainning1.demo.user.serialize.IntegerSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class Segundo implements IntegerSerialization {
    Integer segundo;

    private Segundo(Integer segundo){
        Precondiciones.conditionNoNul(segundo);
        Precondiciones.conditionSegundo(segundo);
        this.segundo = segundo;
    }

    @Override
    public Integer valueOf() {
        return segundo;
    }
}

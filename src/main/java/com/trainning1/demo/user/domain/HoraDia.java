package com.trainning1.demo.user.domain;

public class HoraDia {
    Integer hora;
    Integer minutos;
    Integer segundo;
    Long id;

    public HoraDia(Hora hora, Minuto minuto, Segundo segundo, Long id){
        this.hora= hora.getHora();
        this.minutos = minuto.getMinuto();
        this.segundo = segundo.getSegundo();
        this.id = id;
    }
}

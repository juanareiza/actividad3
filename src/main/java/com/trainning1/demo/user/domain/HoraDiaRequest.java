package com.trainning1.demo.user.domain;

import lombok.Data;

@Data
public class HoraDiaRequest {
    Integer hora;
    Integer minuto;
    Integer segundo;

}

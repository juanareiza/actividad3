package com.trainning1.demo.user.service;

import com.trainning1.demo.user.dao.InMemoryUserRepository;
import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.HoraDia;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@AllArgsConstructor
public class UserService {
    InMemoryUserRepository inMemoryUserRepository;

    public Map<Long, HoraDia> findAll(){
      return inMemoryUserRepository.findAll();

    }
    public HoraDia insertOne(Integer horaRe, Integer minutoRe, Integer segundoRe){
        Hora hora = Hora.of(horaRe);
        Minuto minuto = Minuto.of(minutoRe);
        Segundo segundo = Segundo.of(segundoRe);
        return inMemoryUserRepository.insertOne(hora, minuto, segundo);
    }
}

package com.trainning1.demo.user.controller;

import com.trainning1.demo.user.domain.HoraDia;
import com.trainning1.demo.user.domain.HoraDiaRequest;
import com.trainning1.demo.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/")
public class UserController {
    UserService userService;

    @GetMapping("times")
    public Map<Long, HoraDia> findAll(){
        return userService.findAll();
    }
    @PostMapping("times")
    public HoraDia insertOne(@RequestBody HoraDiaRequest horadia){
        return userService.insertOne(horadia.getHora(),horadia.getMinuto(),horadia.getSegundo());
    }
}
package com.trainning1.demo.user.serialize;

public interface IntegerSerialization {
    Integer valueOf();
}

package com.trainning1.demo.configuracion;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.trainning1.demo.user.domain.Hora;
import com.trainning1.demo.user.domain.Minuto;
import com.trainning1.demo.user.domain.Segundo;
import com.trainning1.demo.user.serialize.IntegerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GsonConfiguration {


    @Bean
    public Gson gson(){

        return new GsonBuilder()
                .registerTypeAdapter(Hora.class, new IntegerAdapter<>(Hora::of))
                .registerTypeAdapter(Minuto.class, new IntegerAdapter<>(Minuto::of))
                .registerTypeAdapter(Segundo.class, new IntegerAdapter<>(Segundo::of))
                .create();
    }
}
